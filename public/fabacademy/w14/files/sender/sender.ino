#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>

const int pinCE = 9;
const int pinCSN = 10;
RF24 radio(pinCE, pinCSN);
 
const uint64_t pipe = 47474747;
 
char data[300]="Hola k ase Laura," ;
char data2[300]="esta es la luz que hay" ;
char data3[300];

String ldr;
 
void setup(void)
{
   Serial.begin(57600);
   radio.begin();
   radio.openWritingPipe(pipe);
}
 
void loop(void)
{
   Serial.println(analogRead(0));
   radio.write(data, sizeof data);
   delay(1000);
   radio.write(data2, sizeof data2);
   radio.write(data3, sizeof data3);
   
   Serial.println(data);
   Serial.println(data2);
   delay(1000);
}
